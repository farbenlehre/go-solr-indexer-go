package main

/**
@author jerome

the script indexes a offline solr core and swap the cores
	1 delete the olde core by querry
	2 indexing from a dir with READY solr xml files like add/doc/file
	3 reload new indexed offline core so changes are on the core
	4 core swap
	5 reload new online core or better reload both cores
**/

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

func main() {
	fmt.Println("start ...")
	pathToSolr := "http://yourname:password@dev.gflsolr.sub.uni-goettingen.de/solr/"
	pathToSolrXmlDir := "./../SolrXML"
	coreOnline := "gfl"
	coreOffline := "gfloffline"
	deleteOfflineCore(pathToSolr, coreOffline)
	indexFromDir(pathToSolr, coreOffline, pathToSolrXmlDir)
	reloadCore(pathToSolr, coreOffline)
	coreSwap(pathToSolr, coreOnline, coreOffline)
	reloadCore(pathToSolr, coreOnline)
	reloadCore(pathToSolr, coreOffline)
	fmt.Println("... finish")
}

//list all files in dir and index solr with them
func indexFromDir(pathToSolr string, core string, pathToSolrXmlDir string) {
	fmt.Println("start indexing ...")
	files, err := ioutil.ReadDir(pathToSolrXmlDir)
	if err != nil {
		fmt.Println("dir or files not found")
		log.Fatal(err)
	}

	fmt.Println("remove old logfile ...")
	if _, err := os.Stat("logfile"); err == nil {
		e := os.Remove("logfile")
		if e != nil {
			log.Fatal(e)
			fmt.Println("error at remove old logfile ...")
		}
	} else if os.IsNotExist(err) {
		fmt.Println("there is no olde logfile ...")
	}

	for _, xmlFile := range files {
		pathToXml := "./../SolrXML/" + xmlFile.Name()
		fmt.Println("indexing " + xmlFile.Name())

		xmlFile, err := os.Open(pathToXml)
		if err != nil {
			fmt.Println(pathToXml + " not found")
			log.Fatal(err)
		}
		defer xmlFile.Close()
		req, err := http.NewRequest("POST", os.ExpandEnv(pathToSolr+core+"/update"), xmlFile)
		if err != nil {
			fmt.Println("Problem at build new POST request")
			log.Fatal(err)
		}
		req.Header.Set("Content-Type", "text/xml")

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			fmt.Println("error at send post request")
			log.Fatal(err)
		}
		if resp.StatusCode != http.StatusOK {
			fmt.Println(resp)
			bodyBytes, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				log.Fatal(err)
			}
			bodyString := string(bodyBytes)
			fmt.Print(bodyString)
			f, err := os.OpenFile("logfile", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
			if err != nil {
				log.Fatalf("error opening file: %v", err)
			}
			defer f.Close()

			log.SetOutput(f)
			log.Println(bodyString)
		}
		defer resp.Body.Close()
	}
}

//delete the index of the offline core by querry
func deleteOfflineCore(pathToSolr string, core string) {
	delQuerry := strings.NewReader("<delete><query>*:*</query></delete>")
	req, err := http.NewRequest("POST", os.ExpandEnv(pathToSolr+core+"/update"), delQuerry)
	if err != nil {
		fmt.Println("Problem at build new POST request")
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "text/xml")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println("error at delete olde core")
		log.Fatal(err)
	}
	defer resp.Body.Close()
}

// reloading the core for changes work
func reloadCore(pathToSolr string, core string) {
	fmt.Println("reload new index core ...")

	resp, err := http.Get(os.ExpandEnv(pathToSolr + "admin/cores?action=RELOAD&core=" + core))
	if err != nil {
		fmt.Println("error at reload core")
		log.Fatal(err)
	}
	defer resp.Body.Close()
}

// change the offline (new index core) with the online (get the old core)
func coreSwap(pathToSolr string, coreOnline string, coreOffline string) {
	fmt.Println("swap cores ...")

	resp, err := http.Get(os.ExpandEnv(pathToSolr + "admin/cores?action=SWAP&core=" + coreOnline + "&other=" + coreOffline))
	if err != nil {
		fmt.Println("error at core swap")
		log.Fatal(err)
	}
	defer resp.Body.Close()
}

